variable "network_peerings" {
  type    = list(map(string))
  default = []
}