resource "google_compute_network_peering" "peering" {
  // use this syntax because for_each just support set or map
  for_each = { for np in var.network_peerings : np.network => np }

  name         = "${basename(each.value.network)}-${basename(each.value.peer_network)}-peering"
  network      = each.value.network
  peer_network = each.value.peer_network
}
